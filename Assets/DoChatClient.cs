﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Text;
using System.IO;
using UnityEngine.UI;


public class DoChatClient : MonoBehaviour {

	public InputField inp;

	public string host = "127.0.0.1";
	public int port = 8080;
	public string nick = "qwe";

	TcpClient client;
	const int READ_BUFFER_SIZE = 255;
	private byte[] readBuffer = new byte[READ_BUFFER_SIZE];
	public string strMessage=string.Empty;
	public string res=String.Empty;

	void Start () {
		client = new TcpClient(host,port);
		Debug.Log("Connected to SERVER");
		SendData(nick);
		Debug.Log("Set Nickname "+nick);
		Debug.Log("Begin read...");
		client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);

		///SendData("status");
		//SendData(nick);
		//Invoke("SendNick",1);
	}
	public void Send()
	{
		SendData(inp.text);
		inp.text = "";
	}

	private void DoRead(IAsyncResult ar)
	{ 
		int BytesRead;
		try
		{
			// Finish asynchronous read into readBuffer and return number of bytes read.
			BytesRead = client.GetStream().EndRead(ar);
			if (BytesRead < 1) 
			{
				// if no bytes were read server has close.  
				res="Disconnected";
				print("SERVER closed");
				
				return;
			}
			// Convert the byte array the message was saved into, minus two for the
			// Chr(13) and Chr(10)
			strMessage = Encoding.UTF8.GetString(readBuffer);

			ParseMessage(strMessage);
			//ProcessCommands(strMessage);
			// Start a new asynchronous read into readBuffer.
			client.GetStream().BeginRead(readBuffer, 0, READ_BUFFER_SIZE, new AsyncCallback(DoRead), null);
			
		} 
		catch
		{
			res="Disconnected";
		}

	}

	public void ParseMessage(string msg)
	{
		var mas = msg.Split(new string[]{"<>"},StringSplitOptions.RemoveEmptyEntries);
		foreach(var val in mas)
		{
			print("SERVER: "+val);
		}
	}

	void OnApplicationQuit() {
		if(client != null)
			client.Close();
	}

	// Use a StreamWriter to send a message to server.
	private void SendData(string data)
	{
		StreamWriter writer = new StreamWriter(client.GetStream());
		writer.Write(data);
		writer.Flush();
	}
}
