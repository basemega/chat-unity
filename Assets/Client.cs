// Asynchronous Client Socket Example
// http://msdn.microsoft.com/en-us/library/bew39x2a.aspx

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

// State object for receiving data from remote device.
using UnityEngine;


public class StateObject {
    // Client socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 256;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
}

public class AsyncClient {

	/* Beeee
    // ManualResetEvent instances signal completion.
    private static ManualResetEvent connectDone = 
        new ManualResetEvent(false);
    private static ManualResetEvent sendDone = 
        new ManualResetEvent(false);
    private static ManualResetEvent receiveDone = 
        new ManualResetEvent(false);
*/


	public string host = "127.0.0.1";
	public int port = 8080;

	private Socket client;

	public event Action OnConnected = delegate{};
	public event Action OnDisconnected = delegate{};
	public event Action OnSended = delegate{};
	public event Action<string> OnReceived = delegate{};

	public AsyncClient(string host, int port)
	{
		this.host = host;
		this.port = port;
	}

	public void Disconnect()
	{
		try
		{
			if(client == null) return;

			if (!client.Connected) return;

			Debug.Log("Disconnected from SERVER");
			OnDisconnected();
			
			client.Shutdown(SocketShutdown.Both);
			client.Close();
		}
		catch (SocketException)
		{
		}
	}

	public void Connect(Action act=null)
	{
		act+= ()=> {
			Receive();
		};

		IPHostEntry ipHostInfo = Dns.GetHostEntry(host);
		IPAddress ipAddress = ipHostInfo.AddressList[0];
		IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
		
		client = new Socket(AddressFamily.InterNetwork,
		                           SocketType.Stream, ProtocolType.Tcp);


		client.BeginConnect( remoteEP, 
		                    new AsyncCallback(ConnectCallback), act);


	}

	private void ConnectCallback(IAsyncResult ar) {
		try {

			client.EndConnect(ar);

			Action act = (Action)ar.AsyncState;
			if(act!=null) act();
			
			//Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());
			Debug.Log("Connected to SERVER");
			
			OnConnected();

		} catch (Exception e) {
			Console.WriteLine(e.ToString());
		}
	}

	public void Send(String data, Action act = null) {
		byte[] byteData = Encoding.UTF8.GetBytes(data);
		
		client.BeginSend(byteData, 0, byteData.Length, 0,
		                 new AsyncCallback(SendCallback), act);

	}

	private void SendCallback(IAsyncResult ar) {
		try {

			int bytesSent = client.EndSend(ar);

			Action act = (Action)ar.AsyncState;
			if(act!=null) act();

			//Debug.Log(string.Format("Sent {0} bytes to server.", bytesSent));

			OnSended();
		} catch (Exception e) {
		}
	}

	private void Receive() {
		try {
			StateObject state = new StateObject();
			state.workSocket = client;
			
			client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
			                    new AsyncCallback(ReceiveCallback), state);
		} catch (Exception e) {
		}
	}

	private void ReceiveCallback( IAsyncResult ar ) {
		try {
			StateObject state = (StateObject) ar.AsyncState;
			
			int bytesRead = client.EndReceive(ar);
			
			if (bytesRead > 0) {
				string msg = Encoding.UTF8.GetString(state.buffer,0,bytesRead);


				OnReceived(msg);

				client.BeginReceive(state.buffer,0,StateObject.BufferSize,0,
				                    new AsyncCallback(ReceiveCallback), state);
			}else
			{
				Disconnect();
			}
		} catch (Exception e) {
		}
	}
	

}