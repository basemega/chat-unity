﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;
using System.Text;
using System.IO;

public static class ChatClient {

	public static string HOST = "127.0.0.1";
	public static int PORT = 8080;
	public static string NICKNAME = "qwe";

	static AsyncClient client;

	static ChatClient()
	{
		client = new AsyncClient(HOST,PORT);
		client.OnReceived += (msg) => {
			ParseMessage(msg);
		};
	}

	public static void Connect()
	{
		client.Connect(()=>{
			client.Send(NICKNAME);
			Debug.Log("Your nick - "+NICKNAME);
		});
	}

	public static void Disconnect()
	{
		client.Disconnect();
	}

	public static void Send(string msg)
	{
		client.Send(msg);
	}

	public static void ParseMessage(string msg)
	{
		var mas = msg.Split(new string[]{"<>"},StringSplitOptions.RemoveEmptyEntries);
		foreach(var val in mas)
		{
			Debug.Log("SERVER: "+val);
		}
	}
}
