﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Test : MonoBehaviour {

	public InputField inp;
	
	// Use this for initialization
	void Start () {
		ChatClient.Connect();
	}

	public void Send()
	{
		ChatClient.Send(inp.text);
		inp.text = "";
	}
	
	void OnApplicationQuit() {
		ChatClient.Disconnect();
	}
}
